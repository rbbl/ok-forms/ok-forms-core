terraform {
  required_providers {
    auth0 = {
      source  = "auth0/auth0"
      version = "~> 0.34.0"
    }
  }
}

provider "auth0" {}


resource "auth0_resource_server" "core_api" {
  name        = "Core-API"
  identifier  = "core-api"
  signing_alg = "RS256"

  allow_offline_access                            = true
  token_lifetime                                  = 8600
  skip_consent_for_verifiable_first_party_clients = true

  scopes {
    value       = "read:contacts"
    description = "Read Contacts"
  }

  scopes {
    value       = "write:contacts"
    description = "Write Contacts"
  }

  scopes {
    value       = "delete:contacts"
    description = "Delte Contacts"
  }
}

resource "auth0_client" "core_api_test_app" {
  name     = "Core-API Test Application"
  app_type = "non_interactive"
}

resource "auth0_client_grant" "my_client_grant" {
  client_id = auth0_client.core_api_test_app.id
  audience  = auth0_resource_server.core_api.identifier
  scope     = ["read:contacts", "write:contacts", "delete:contacts"]
}