# OK-Forms Core
## Dev Setup Podman
the current skaffold.yaml is for a Podman + Minikube Setup. If you want something else pls set up your own skaffold.yaml .

**Prerequisites**: 
1. `export DOCKER_HOST=unix:///run/user/$UID/podman/podman.sock` to set Podman as DOCKER_HOST 
2. follow the [knative Quickstart](https://knative.dev/docs/install/quickstart-install/) Guide and be sure to use Minikube
3. `minikube profile knative` to set knative as active minikube profile
4. [install Skaffold](https://skaffold.dev/docs/install/#standalone-binary)

**Run**
1. `minikube tunnel` (if you did not set knative as your active then you need to specify the `--profile` flag )
2. `skaffold run` to launch the Application
3. `kubectl get services.serving.knative.dev` to get the API URL