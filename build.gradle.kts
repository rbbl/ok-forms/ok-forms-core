import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar

val ktorVersion="2.1.1"
val kotlinVersion="1.7.10"
val logbackVersion="1.2.10"
val exposedVersion="0.37.3"

plugins {
    application
    kotlin("jvm") version "1.7.10"
    kotlin("plugin.serialization") version "1.7.10"
    id("com.github.johnrengelman.shadow") version "7.1.2"
    id("org.openapi.generator") version "5.4.0"
}

group = "cc.rbbl.okforms"
version = "0.0.1"

application {
    mainClass.set("cc.rbbl.okforms.core.ApplicationKt")
}

sourceSets {
    main {
        java {
            srcDir("src-gen/src/main/kotlin")
        }
    }
}

repositories {
    mavenCentral()
    maven("https://gitlab.com/api/v4/projects/28863171/packages/maven")
}

dependencies {
    implementation("cc.rbbl:program-parameters-jvm:1.0.3")
    implementation("cc.rbbl:ktor-health-check:2.0.0")
    implementation("org.postgresql:postgresql:42.5.0")
    implementation("org.flywaydb:flyway-core:8.5.0")
    implementation("org.jetbrains.kotlinx:kotlinx-datetime:0.3.1")
    implementation("org.jetbrains.exposed:exposed-core:$exposedVersion")
    implementation("org.jetbrains.exposed:exposed-dao:$exposedVersion")
    implementation("org.jetbrains.exposed:exposed-java-time:$exposedVersion")
    implementation("org.jetbrains.exposed:exposed-jdbc:$exposedVersion")
    implementation("io.ktor:ktor-server-core:$ktorVersion")
    implementation("io.ktor:ktor-server-netty:$ktorVersion")
    implementation("io.ktor:ktor-serialization:$ktorVersion")
    implementation("io.ktor:ktor-server-auth:$ktorVersion")
    implementation("io.ktor:ktor-server-auth-jwt:$ktorVersion")
    implementation("io.ktor:ktor-server-content-negotiation:$ktorVersion")
    implementation("io.ktor:ktor-serialization-kotlinx-json:$ktorVersion")
    implementation("ch.qos.logback:logback-classic:$logbackVersion")
    testImplementation("io.ktor:ktor-server-tests:$ktorVersion")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.6.4")
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit:$kotlinVersion")
}


tasks.getByName<ShadowJar>("shadowJar") {
    archiveFileName.set("ok-forms-core.jar")
}

tasks.findByName("clean")?.doFirst {
    delete("src-gen")
}

openApiGenerate {
    generatorName.set("kotlin-server")
    inputSpec.set("$rootDir/openapi.yaml")
    outputDir.set("$rootDir")
    modelPackage.set("cc.rbbl.contact.server.dtos")
    globalProperties.set(mapOf("models" to ""))
    configOptions.set(mapOf("serializableModel" to "true"))
    generateModelDocumentation.set(false)
}