CREATE TABLE contacts
(
    id            SERIAL UNIQUE,
    first_name    varchar(50),
    middle_name   varchar(50),
    last_name     varchar(50),
    pseudonym     varchar(100),
    date_of_birth date,
    phone         varchar(10),
    mail          varchar(200),
    website       varchar(100),
    company       varchar(100),
    position      varchar(50),
    picture_url   varchar(1024)
);