package cc.rbbl.okforms.core

import cc.rbbl.ktor_health_check.Health
import cc.rbbl.okforms.core.plugins.configureAuth
import cc.rbbl.okforms.core.plugins.configureRouting
import io.ktor.serialization.kotlinx.json.*
import io.ktor.server.application.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import io.ktor.server.plugins.contentnegotiation.*
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.flywaydb.core.Flyway
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.transactions.transaction
import org.slf4j.LoggerFactory

const val defaultPort = 8080

fun main(args: Array<String>) {

    val log = LoggerFactory.getLogger("Main Function")
    ProgramConfig.parameters.loadParametersFromEnvironmentVariables()
    ProgramConfig.parameters.loadParameters(args)
    ProgramConfig.parameters.checkParameterCompleteness()
    val params = ProgramConfig()
    Flyway.configure().dataSource(params.jdbcUrl, params.dbUser, params.dbPassword).load().migrate()
    Database.connect(params.jdbcUrl, "org.postgresql.Driver", params.dbUser, params.dbPassword)
    val port = try {
        params.apiPort?.toInt()
    } catch (e: NumberFormatException) {
        log.warn("Provided Port '${params.apiPort}' is not an Integer. Using default Port $defaultPort.")
        null
    }
    runBlocking {
        launch {
            transaction {
                HealthAttributes.database = !connection.isClosed
                if (!HealthAttributes.database) {
                    log.warn("DB health check failed")
                }else {
                    log.info("DB health check succeeded")
                }
            }
            delay(10000)
        }
        launch {
            embeddedServer(Netty, port = port ?: defaultPort, host = "0.0.0.0") {
                install(ContentNegotiation) {
                    json()
                }
                configureAuth()
                configureRouting()
                install(Health) {
                    readyCheck("database") {
                        HealthAttributes.database
                    }
                    healthCheck("database") {
                        HealthAttributes.database
                    }
                }
            }.start(wait = true)
        }
    }
}
