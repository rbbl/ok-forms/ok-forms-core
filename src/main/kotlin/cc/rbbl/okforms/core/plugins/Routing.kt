package cc.rbbl.okforms.core.plugins

import cc.rbbl.okforms.core.services.ContactService
import cc.rbbl.okforms.core.services.ContactServiceImpl
import cc.rbbl.okforms.core.services.MessageService
import cc.rbbl.okforms.core.services.MessageServiceImpl
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

fun Application.configureRouting() {
    val contactService: ContactService = ContactServiceImpl()
    val messageService: MessageService = MessageServiceImpl()

    routing {
        route("/contacts") {
            authenticate("auth0-read-contacts") {
                get {
                    call.respond(contactService.getAllContacts())
                }
            }
            authenticate("auth0-write-contacts") {
                post {
                    call.respond(contactService.createContact(call.receive()))
                }
            }
            route("/{id}") {
                get {
                    call.respond(contactService.getContact(call.parameters["id"]!!.toInt()))
                }
                authenticate("auth0-write-contacts") {
                    put {
                        call.respond(contactService.updateContact(call.parameters["id"]!!.toInt(), call.receive()))
                    }
                }
            }
        }

        route("/message") {
            post {
                call.respond(messageService.createMessage(call.receive()))
            }
        }
    }
}
