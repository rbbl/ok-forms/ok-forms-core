package cc.rbbl.okforms.core.plugins

import cc.rbbl.okforms.core.ProgramConfig
import com.auth0.jwk.JwkProviderBuilder
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.auth.jwt.*

import java.util.concurrent.TimeUnit

fun Application.configureAuth() {
    val config = ProgramConfig()
    val jwkProvider = JwkProviderBuilder(config.issuerUrl)
        .cached(10, 24, TimeUnit.HOURS)
        .rateLimited(10, 1, TimeUnit.MINUTES)
        .build()

    install(Authentication) {
        jwt("auth0") {
            verifier(jwkProvider, config.issuerUrl)
            validate { credential -> validateCredentials(credential, config) }
        }
        jwt("auth0-read-contacts") {
            verifier(jwkProvider, config.issuerUrl)
            validate { credential -> validateCredentials(credential, config, "read:contacts") }
        }
        jwt("auth0-write-contacts") {
            verifier(jwkProvider, config.issuerUrl)
            validate { credential -> validateCredentials(credential, config, "write:contacts") }
        }
        jwt("auth0-delete-contacts") {
            verifier(jwkProvider, config.issuerUrl)
            validate { credential -> validateCredentials(credential, config, "delete:contacts") }
        }
    }
}

fun validateCredentials(credential: JWTCredential, config: ProgramConfig, scope: String? = null): JWTPrincipal? {
    val containsAudience = credential.payload.audience.contains(config.audience)
    println(credential.payload.claims["scope"])
    println(credential.payload.claims["scope"]?.asString()?.split(" "))
    val containsScope = scope.isNullOrBlank() ||
            credential.payload.claims["scope"]?.asString()?.split(" ")?.contains(scope) == true
    if (containsAudience && containsScope) {
        return JWTPrincipal(credential.payload)
    }

    return null
}