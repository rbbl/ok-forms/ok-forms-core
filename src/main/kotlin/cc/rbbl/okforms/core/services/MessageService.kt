package cc.rbbl.okforms.core.services

import cc.rbbl.okforms.core.dtos.Message
import cc.rbbl.okforms.core.dtos.MessageCreate

interface MessageService {
    fun createMessage(message: MessageCreate): Message
}