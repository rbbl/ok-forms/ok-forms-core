package cc.rbbl.okforms.core.services

import cc.rbbl.okforms.core.dtos.Contact
import cc.rbbl.okforms.core.dtos.ContactCreate

interface ContactService {
    fun getContact(id: Int): Contact
    fun getAllContacts(): Array<Contact>
    fun createContact(contact: ContactCreate): Contact
    fun updateContact(id: Int, contact: Contact): Contact
    fun deleteContact(id: Int)
}