package cc.rbbl.okforms.core.services

import cc.rbbl.okforms.core.dtos.Contact
import cc.rbbl.okforms.core.dtos.ContactCreate
import cc.rbbl.okforms.core.persistence.ContactEntity
import kotlinx.datetime.toJavaLocalDate
import org.jetbrains.exposed.sql.transactions.transaction

class ContactServiceImpl : ContactService {
    override fun getContact(id: Int): Contact {
        return transaction {
            ContactEntity[id]
        }.toDto()
    }

    override fun getAllContacts(): Array<Contact> {
        return transaction {
            ContactEntity.all().map { it.toDto() }
        }.toTypedArray()
    }

    override fun createContact(contact: ContactCreate): Contact {
        val newContact = transaction {
            ContactEntity.new {
                firstName = contact.firstName
                middleName = contact.middleName
                lastName = contact.lastName
                pseudonym = contact.pseudonym
                dateOfBirth = contact.dateOfBirth?.toJavaLocalDate()
                phone = contact.phone
                mail = contact.mail
                website = contact.website
                company = contact.company
                position = contact.position
                pictureUrl = contact.pictureUrl
            }
        }
        return newContact.toDto()
    }

    override fun updateContact(id: Int, contact: Contact): Contact {
        return transaction {
            val dbContact = ContactEntity[id]
            dbContact.firstName = contact.firstName
            dbContact.middleName = contact.middleName
            dbContact.lastName = contact.lastName
            dbContact.pseudonym = contact.pseudonym
            dbContact.dateOfBirth = contact.dateOfBirth?.toJavaLocalDate()
            dbContact.phone = contact.phone
            dbContact.mail = contact.mail
            dbContact.website = contact.website
            dbContact.company = contact.company
            dbContact.position = contact.position
            dbContact.pictureUrl = contact.pictureUrl
            dbContact
        }.toDto()
    }

    override fun deleteContact(id: Int) {
        transaction {
            ContactEntity[id].delete()
        }
    }

}