package cc.rbbl.okforms.core

import kotlinx.datetime.DatePeriod
import kotlinx.datetime.LocalDate
import kotlinx.datetime.minus

operator fun LocalDate.minus(other: LocalDate?): DatePeriod? {
    if(other == null){
        return null
    }
    return this - other
}