package cc.rbbl.okforms.core.persistence

import cc.rbbl.okforms.core.dtos.Contact
import cc.rbbl.okforms.core.minus
import kotlinx.datetime.*
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID

class ContactEntity(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<ContactEntity>(Contacts)

    var firstName by Contacts.firstName
    var middleName by Contacts.middleName
    var lastName by Contacts.lastName
    var pseudonym by Contacts.pseudonym
    var dateOfBirth by Contacts.dateOfBirth
    var phone by Contacts.phone
    var mail by Contacts.mail
    var website by Contacts.website
    var company by Contacts.company
    var position by Contacts.position
    var pictureUrl by Contacts.pictureUrl

    fun toDto(): Contact {
        val age = (Clock.System.todayAt(TimeZone.UTC) - dateOfBirth?.toKotlinLocalDate())?.years
        return Contact(
            id.value,
            firstName,
            middleName,
            lastName,
            pseudonym,
            dateOfBirth?.toKotlinLocalDate(),
            age,
            phone,
            mail,
            website,
            company,
            position,
            pictureUrl
        )
    }
}