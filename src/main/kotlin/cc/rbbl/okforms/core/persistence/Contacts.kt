package cc.rbbl.okforms.core.persistence

import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.javatime.date

object Contacts : IntIdTable("contacts") {
    val firstName = varchar("first_name", 50).nullable()
    val middleName = varchar("middle_name", 50).nullable()
    val lastName = varchar("last_name", 50).nullable()
    val pseudonym = varchar("pseudonym", 100).nullable()
    val dateOfBirth = date("date_of_birth").nullable()
    val phone = varchar("phone", 10).nullable()
    val mail = varchar("mail", 200).nullable()
    val website = varchar("website", 100).nullable()
    val company = varchar("company", 100).nullable()
    val position = varchar("position", 50).nullable()
    val pictureUrl = varchar("picture_url", 1024).nullable()
}
