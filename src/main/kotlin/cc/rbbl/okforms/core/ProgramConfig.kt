package cc.rbbl.okforms.core

import cc.rbbl.program_parameters_jvm.ParameterDefinition
import cc.rbbl.program_parameters_jvm.ParameterHolder

class ProgramConfig {
    companion object {
        val parameters = ParameterHolder(
            setOf(
                ParameterDefinition("JDBC_URL", required = true),
                ParameterDefinition("DB_USER", required = true),
                ParameterDefinition("DB_PASSWORD", required = true),
                ParameterDefinition("ISSUER_URL", required = true),
                ParameterDefinition("AUDIENCE", required = true),
                ParameterDefinition("API_PORT", required = false)
            )
        )
    }


    val jdbcUrl: String = parameters["JDBC_URL"]!!
    val dbUser: String = parameters["DB_USER"]!!
    val dbPassword: String = parameters["DB_PASSWORD"]!!
    val issuerUrl: String = parameters["ISSUER_URL"]!!
    val audience: String = parameters["AUDIENCE"]!!
    val apiPort: String? = parameters["API_PORT"]
}