FROM gradle:7.5.1-jdk17-alpine
COPY . /app
WORKDIR /app
RUN gradle build --no-daemon

FROM openjdk:17-alpine
COPY --from=0 /app/build/libs/ok-forms-core.jar /app/ok-forms-core.jar

CMD ["java", "-jar", "/app/ok-forms-core.jar"]